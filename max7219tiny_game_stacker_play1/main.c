/**
 * Game-Stacker - Tinusaur Stacker Game for LED 8x8 Matrix
 * @author Neven Boyanov
 * This is part of the Tinusaur/MAX7219tiny project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2023 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/max7219tiny-game-stacker
 */

#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "tinyavrlib/soundlib.h"
#include "tinyavrlib/soundlib_notes.h"

#include "max7219tiny/max7219tiny.h"
// To reassign the DIN/CS/CLK to different I/O pins you must edit the library code
// Find the definitions in the "max7219tiny.h" file in the MAX7219Tiny library.
// IMPORTANT/NOTE: Do that ONLY of you know what you are doing!

#include "shield_gamex3_scores/melody_15611.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                 ATtiny85
//               +----------+   (-)--GND--
//       (RST)---+ PB5  Vcc +---(+)--VCC--
//  --[OWOWOD]---+ PB3  PB2 +------[DIN]--
//  -------------+ PB4  PB1 +-------[CS]--
//  --GND--(-)---+ GND  PB0 +------[CLK]--
//               +----------+
//              Tinusaur Board
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#define MAX7219_SEG_NUM 2	// The number of the segments. Increase this for multiple matrices.
#define MAX7219_SEG_LAST (MAX7219_SEG_NUM - 1) * 8	// The index in the buffer of the last segment.
#define MAX7219_BUFFER_SIZE	MAX7219_SEG_NUM * 8		// The size of the buffer

uint8_t max7219_buffer[MAX7219_BUFFER_SIZE];

// ----------------------------------------------------------------------------

#define STACKERGAME_BUTTON_PORT		PB3

// ----------------------------------------------------------------------------
// Bit-reverse routine
// REF: http://omarfrancisco.com/reversing-bits-in-a-byte/
// REF: http://stackoverflow.com/questions/15430135/how-to-reverse-a-byte
static inline uint8_t bits_reverse(uint8_t x) {
    x = ((x & 0x55) << 1) | ((x & 0xaa) >> 1);
    x = ((x & 0x33) << 2) | ((x & 0xcc) >> 2);
    x = ((x & 0x0f) << 4) | ((x & 0xf0) >> 4);
    return x;
}

// ----------------------------------------------------------------------------

uint8_t bars[] = {
	0b000000001,	// 0: 1
	0b000000011,	// 1: 2
	0b000000111,	// 2: 3
	0b000001111,	// 3: 4
	0b000011111,	// 4: 5
	0b000111111,	// 5: 6
	0b001111111,	// 6: 7
	0b011111111,	// 7: 8
};
//     ====-^^^^^^-====
//     5432109876543210

uint8_t data_smile[] = {
	0b00111100,
	0b01000010,
	0b10100101,
	0b10000001,
	0b10100101,
	0b10011001,
	0b01000010,
	0b00111100,
};

uint8_t data_levels[][8] = {
	{	0b00000000,	// Level-1
		0b00010000,
		0b00011000,
		0b00011100,
		0b00011000,
		0b00011000,
		0b00111100,
		0b00000000,	},
	{	0b00000000,	// Level-2
		0b00011000,
		0b00100100,
		0b00100000,
		0b00011000,
		0b00000100,
		0b00111100,
		0b00000000,	},
	{	0b00000000,	// Level-3
		0b00011000,
		0b00100100,
		0b00010000,
		0b00100000,
		0b00100100,
		0b00011000,
		0b00000000,	},
};

// ----------------------------------------------------------------------------

void display_set(void) {
	for (uint8_t i = 0; i <= 7; i++) max7219b_col(i, 0xff);
}

void display_clr(void) {
	for (uint8_t i = 0; i <= 7; i++) max7219b_col(i, 0);
}

void display_blink(void) {
	for (uint8_t i = 1; i <= 20; i++) {
		display_set();
		_delay_ms(50);
		display_clr();
		_delay_ms(50);
	}
}

void display_out(uint8_t *data) {
	uint8_t c = 0;
	uint8_t i = 7;
	for (;;) {
		max7219b_col(c++, data[i]);
		if (i == 0) break;
		i--;
	}
}

void display_smile(void) {
	display_out(data_smile);
}

void display_smile_blink(void) {
	for (uint8_t i = 1; i <= 12; i++) {
		display_smile();
		_delay_ms(50);
		display_clr();
		_delay_ms(50);
	}
}

void display_level_blink(uint8_t l) {
	for (uint8_t i = 1; i <= 6; i++) {
		display_out(data_levels[l]);
		_delay_ms(100);
		display_clr();
		_delay_ms(100);
	}
}

void bar_out(uint8_t len, int8_t pos, int8_t row) {
	uint8_t bar = bars[len - 1];
	int8_t sh = pos - len + 1;
	if (sh < 0) { // should shift right
		sh = - sh; // change sign
		bar = bar >> sh; // do shift
	} else if (sh > 0) { // should shift left
		bar = bar << sh; // do shift
	} // sh == 0, no shift
	bar = bits_reverse(bar); // Reverse bits for more convenient visualization
	max7219b_col(row, bar);
}

void bar_down(uint8_t len, int8_t pos, int8_t row, int8_t down) {
	while (1) {
		bar_out(len, pos, row);
		_delay_ms(80);
		if (row <= down) break;
		max7219b_col(row--, 0);
	}
}

uint8_t bar_data(uint8_t row) {
	return max7219b_get(row);
}

void level_speed_delay(uint8_t s) {
	for (uint8_t i = 4; i > s; i--) _delay_ms(12);
}

// ----------------------------------------------------------------------------

#define DROP_NOTE_LEN NOTE_32TD
soundlib_melody_data_t sound_drop_notes[] PROGMEM = {
    DROP_NOTE_LEN | NOTE_G6S, DROP_NOTE_LEN | NOTE_G6N, DROP_NOTE_LEN | NOTE_F6S, DROP_NOTE_LEN | NOTE_F6N,
    DROP_NOTE_LEN | NOTE_E6N, DROP_NOTE_LEN | NOTE_D6S, DROP_NOTE_LEN | NOTE_D6N, DROP_NOTE_LEN | NOTE_C6S,
    DROP_NOTE_LEN | NOTE_C6N, DROP_NOTE_LEN | NOTE_B6N, DROP_NOTE_LEN | NOTE_A6S, DROP_NOTE_LEN | NOTE_A6N,
    DROP_NOTE_LEN | NOTE_G5S, DROP_NOTE_LEN | NOTE_G5N, DROP_NOTE_LEN | NOTE_F5S, DROP_NOTE_LEN | NOTE_F5N,
    DROP_NOTE_LEN | NOTE_E5N, DROP_NOTE_LEN | NOTE_D5S, DROP_NOTE_LEN | NOTE_D5N, DROP_NOTE_LEN | NOTE_C5S,
	};

#define LOSE_NOTE_LEN NOTE_02TH
soundlib_melody_data_t sound_lose_notes[] PROGMEM = {
    LOSE_NOTE_LEN | NOTE_C3N, LOSE_NOTE_LEN | NOTE_B3N, LOSE_NOTE_LEN | NOTE_A3S, LOSE_NOTE_LEN | NOTE_A3N,
	};

#define WIN_NOTE_LEN NOTE_16TH
soundlib_melody_data_t sound_win_notes[] PROGMEM = {
    WIN_NOTE_LEN | NOTE_A4N, WIN_NOTE_LEN | NOTE_A4S, WIN_NOTE_LEN | NOTE_B4N, WIN_NOTE_LEN | NOTE_C4N,
    WIN_NOTE_LEN | NOTE_C4S, WIN_NOTE_LEN | NOTE_D4N, WIN_NOTE_LEN | NOTE_D4S, WIN_NOTE_LEN | NOTE_E4N,
    WIN_NOTE_LEN | NOTE_F4N, WIN_NOTE_LEN | NOTE_F4S, WIN_NOTE_LEN | NOTE_G4N, WIN_NOTE_LEN | NOTE_G4S,
	};

// ----------------------------------------------------------------------------

int stacker_sound_pause(void) {
	return soundlib_melody_pause();
}

void stacker_soundtrack(int index) {
	soundlib_melody_resume(melody_15611_notes, sizeof(melody_15611_notes) / sizeof(melody_15611_notes[0]), 1, index);
}

void stacker_sound_drop(void) {
	soundlib_melody_play(sound_drop_notes, sizeof(sound_drop_notes) / sizeof(sound_drop_notes[0]), 7);
}

void stacker_sound_lose(void) {
	soundlib_melody_play(sound_lose_notes, sizeof(sound_lose_notes) / sizeof(sound_lose_notes[0]), 7);
}

void stacker_sound_win(void) {
	soundlib_melody_play(sound_win_notes, sizeof(sound_win_notes) / sizeof(sound_win_notes[0]), 7);
}

// ----------------------------------------------------------------------------

#define MATRIX_NUM		1
#define BAR_START_POS	(MATRIX_NUM*8-1)  // Depends on the number of matrices -> 7: for 1 segment; 15: for 2 segments

// ----------------------------------------------------------------------------

int main(void) {
	// ---- Initialization ----
	scheduler_init();
	scheduler_reinit(SCHEDULER_TCCR0B_1024, SCHEDULER_OCR0A_MIN);	// Adjust, if necessary
	scheduler_start();
	max7219b_init(MAX7219_SEG_NUM, max7219_buffer, MAX7219_BUFFER_SIZE);
	max7219b_scheduler();
	soundlib_init();
	soundlib_scheduler(SOUNDLIB_TEMPO);
	stacker_soundtrack(0);

	// Init: BUTTON
	DDRB &= ~(1 << STACKERGAME_BUTTON_PORT); // Set the BUTTON port as input.
	PORTB |= (1 << STACKERGAME_BUTTON_PORT); // Set the internal pullup.
	
	// ---- Main Loop ----
	uint8_t level = 0;
	for (;;) {
		display_clr();
		uint8_t layer_max = 7;
		if (MATRIX_NUM > 1) layer_max = 9;
		uint8_t layer_win = 5;
		if (MATRIX_NUM > 1) layer_win = 7;
		display_level_blink(level);

		for (uint8_t layer = 0; layer <= layer_max; layer++) {
			uint8_t len = 8 - layer;	// Length of the bar
			uint8_t pos_min = 0;		// Start position.
			uint8_t pos_max = 5 + len;	// End position.
			int8_t pos = pos_min;		// Current position on the left side.
			int8_t step = +1;			// Step. It should be either "+1" or "-1".
			
			#define AUTO_DROP_BAR 1 // How many times until auto-drop a bar. 1=Demo; 3=Play.
			for (uint8_t c = 1; c <= (pos_max << AUTO_DROP_BAR) + ((8 - layer) >> 2) + 5; c++) {
				bar_out(len, pos, BAR_START_POS);
				
				if(!((PINB >> STACKERGAME_BUTTON_PORT) & 1)) break;

				if (pos <= pos_min) step = +1;
				if (pos >= pos_max) step = -1;
				pos += step;

				level_speed_delay(level);
			}
			int sound_pause_index = stacker_sound_pause();
			stacker_sound_drop();
			bar_down(len, pos, BAR_START_POS, layer);
			stacker_soundtrack(sound_pause_index);
			if (layer > 0) {
				if ((bar_data(layer) | bar_data(layer - 1)) != bar_data(layer - 1)) {
					// FAILURE
					stacker_sound_lose();
					display_blink();
					stacker_soundtrack(0);
					if (level > 0) level--;
					break;
				} else {
					// NON-FAILURE
					// Check if it is at the top
					if (layer >= layer_win) {
						int sound_pause_index = stacker_sound_pause();
						stacker_sound_win();
						display_smile_blink();
						stacker_soundtrack(sound_pause_index);
						level++; // Go to the next level.
						if (level > 2) level = 0;
						break;
					}
				}
			}
			_delay_ms(1000);
		}
		display_clr();
		_delay_ms(1000);
	}

	return 0; // Return the mandatory result value. It is "0" for success.
}

// ============================================================================
